class TrelloLabel:
    def __init__(self, label_id, name, color):
        self.id = label_id
        self.name = name
        self.color = color

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)
