from .trellolabel import TrelloLabel


class TrelloCard:
    def __init__(self, name, labels, card_id):
        self.id = card_id
        self.name = name
        self.labels = labels

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)
