class TrelloList:
    def __init__(self, list_id, board_id, name):
        self.id = list_id
        self.board_id = board_id
        self.name = name

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)
