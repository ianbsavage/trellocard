from .trelloclient import TrelloClient
from .trellolabel import TrelloLabel
from .trellolist import TrelloList
from .trellocard import TrelloCard
from .trelloboard import TrelloBoard
import argparse
import logging
import os
import time
import requests
import json
import sys

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
LOG_DIR = os.path.join(ROOT_DIR, '../logs')
if not os.path.exists(LOG_DIR):
    os.makedirs(LOG_DIR)
DEFAULT_API_VERSION = 1
DEFAULT_LOG_LEVEL = logging.INFO


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='A simple program for adding a card to a Trello board'
    )
    parser.add_argument(
        '--list_id',
        type=str,
        help='list ID where the card will be created'
    )
    parser.add_argument(
        '--board_id',
        type=str,
        help='board ID used if a list does not already exist'
    )
    parser.add_argument(
        '--card_name',
        type=str,
        help='name for the card that will be created'
    )
    parser.add_argument(
        '--card_comment',
        type=str,
        help='comment string added to a new card'
    )
    parser.add_argument(
        '--card_label_ids',
        type=str,
        help='label IDs added to a new card. Additional values '
             + 'separated by commas'
    )
    parser.add_argument(
        '--card_label_json',
        type=str,
        help='JSON used to create new labels.'
             + ' Ex: [{"name":"exlabel1","color":"blue"},...]'
    )
    parser.add_argument(
        '--log_level',
        type=str,
        help='log level from DEBUG, INFO, WARNING, ERROR or '
             + 'CRITICAL'
    )
    parser.add_argument(
        '--list_name',
        type=str,
        help='name for a list that will be created'
    )
    return parser.parse_args()


def configure_logger(log_level_arg):
    log_level = DEFAULT_LOG_LEVEL
    if log_level_arg is not None:
        log_level = log_level_arg
    t = time.time()
    timestamp = time.strftime('%Y%m%d-%H%M%S', time.localtime(t))
    log_path = os.path.join(LOG_DIR, f'{timestamp}.log')
    logging.basicConfig(
        filename=log_path,
        level=log_level,
        format='%(asctime)s | %(levelname)s | %(message)s',
        datefmt='%m/%d/%Y %I:%M:%S%p'
    )


def main():
    args = parse_arguments()
    configure_logger(args.log_level)
    if args.list_id is None and args.board_id is None:
        logging.error('Please provide either a list ID or board ID.')
        sys.exit('Please provide either a list ID or board ID.')
    if args.list_id is None and args.list_name is None:
        logging.error('Please provide a list name if creating a new list.')
        sys.exit('Please provide a list name if creating a new list.')

    logging.info('Obtaining key and token from environment variables')
    try:
        api_key = os.environ['TRELLO_API_KEY']
        api_token = os.environ['TRELLO_API_TOKEN']
    except KeyError as e:
        logging.error('Missing key and/or token')
        logging.error(e)
        sys.exit('Missing key and/or token')

    logging.info('Parsing label input')
    label_list = None
    label_json = None
    if args.card_label_ids is not None:
        label_list = args.card_label_ids.split(',')
    elif args.card_label_json is not None:
        try:
            label_json = json.loads(args.card_label_json)
        except json.JSONDecodeError as e:
            logging.error('Parsing label JSON failed')
            logging.error(e)
            logging.error('Exiting program now.')
            sys.exit('Parsing label JSON failed')

    trello_client = TrelloClient(api_key, api_token, DEFAULT_API_VERSION)
    trello_board = TrelloBoard(args.board_id, None)
    trello_list = TrelloList(args.list_id, args.board_id, args.list_name)
    trello_card = TrelloCard(args.card_name, None, None)

    if trello_list.id is None:
        logging.info(
            'Creating new list with name %s in board %s',
            trello_list.name,
            trello_board.id
        )
        json_response = trello_client.create_list(trello_list)
        trello_list.id = json_response['id']
    else:
        json_response = trello_client.get_list(
            trello_list,
            {'fields': 'idBoard,name'})
        trello_list.board_id = json_response['idBoard']
        trello_list.name = json_response['name']

    if trello_board.id is None:
        trello_board.id = trello_list.board_id

    trello_board.list = trello_list
    json_response = trello_client.get_board(trello_board, {'fields': 'name'})
    trello_board.name = json_response['name']

    if label_list is None and label_json is not None:
        logging.info('Creating new labels using input')
        label_list = []
        for label in label_json:
            logging.info(f'Creating label {str(label)}')
            temp_label = TrelloLabel(None, label['name'], label['color'])
            new_label = trello_client.create_label(temp_label, trello_board)
            label_list.append(new_label['id'])

    trello_card.labels = label_list
    logging.info('Creating new card')
    new_card = trello_client.create_card(trello_list, trello_card)
    trello_card.id = new_card['id']

    if args.card_comment is not None:
        logging.info('Adding comment to new card')
        comment = trello_client.create_comment(args.card_comment, trello_card)


if __name__ == '__main__':
    main()
