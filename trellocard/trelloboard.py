from .trellocard import TrelloCard
from .trellolist import TrelloList
from .trellolabel import TrelloLabel


class TrelloBoard:
    def __init__(self, board_id, name):
        self.id = board_id
        self.name = name

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)
