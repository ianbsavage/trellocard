import requests
import logging
import json
import sys
from .trellolabel import TrelloLabel
from .trellolist import TrelloList
from .trellocard import TrelloCard
from .trelloboard import TrelloBoard


class TrelloClient:
    def __init__(self, api_key, api_token, api_version=1):
        self.api_key = api_key
        self.api_token = api_token
        self.api_version = api_version
        self.base_api_url = 'https://api.trello.com'

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def get_board(self, trello_board, fields):
        logging.info('Getting info regarding board %s', trello_board.id)
        querystring = {
            "key": self.api_key,
            "token": self.api_token
        }
        querystring.update(fields)
        response = self.make_request(
            "GET",
            f"boards/{trello_board.id}",
            querystring
        )
        if response.status_code == requests.codes.ok:
            try:
                json_response = json.loads(response.text)
                return json_response
            except json.JSONDecodeError as e:
                logging.error('Parsing response for board %s failed',
                              trello_board.id)
                logging.error(e)
                logging.error('Exiting program.')
                sys.exit(
                    f'Parsing response for board {trello_board.id} failed'
                )
        else:
            logging.error('Request for board %s failed', trello_board.id)
            logging.error('Exiting program now.')
            sys.exit(f'Request for board {trello_board.id} failed')

    def get_list(self, trello_list, fields):
        logging.info('Getting info regarding list %s', trello_list.id)
        querystring = {
            "key": self.api_key,
            "token": self.api_token
        }
        querystring.update(fields)
        response = self.make_request(
            "GET",
            f"lists/{trello_list.id}",
            querystring
        )
        if response.status_code == requests.codes.ok:
            try:
                json_response = json.loads(response.text)
                return json_response
            except json.JSONDecodeError as e:
                logging.error('Parsing response for list %s failed',
                              trello_list.id)
                logging.error(e)
                logging.error('Exiting program.')
                sys.exit(
                    f'Parsing response for list {trello_list.id} failed'
                )
        else:
            logging.error('Request for list %s failed', trello_list.id)
            logging.error('Exiting program now.')
            sys.exit(f'Request for list {trello_list.id} failed')

    def get_label(self, trello_label, fields):
        logging.info('Getting info regarding label %s', trello_label.id)
        querystring = {
            "key": self.api_key,
            "token": self.api_token
        }
        querystring.update(fields)
        response = self.make_request(
            "GET",
            f"labels/{trello_label.id}",
            querystring
        )
        if response.status_code == requests.codes.ok:
            try:
                json_response = json.loads(response.text)
                return json_response
            except json.JSONDecodeError as e:
                logging.error('Parsing response for label %s failed',
                              trello_label.id)
                logging.error(e)
                logging.error('Exiting program.')
                sys.exit(
                    f'Parsing response for label {trello_label.id} failed'
                )
        else:
            logging.error('Request for label %s failed', trello_label.id)
            logging.error('Exiting program now.')
            sys.exit(f'Request for label {trello_label.id} failed')

    def get_card(self, trello_card, fields):
        logging.info('Getting info regarding card %s', trello_card.id)
        querystring = {
            "key": self.api_key,
            "token": self.api_token
        }
        querystring.update(fields)
        response = self.make_request(
            "GET",
            f"cards/{trello_card.id}",
            querystring
        )
        if response.status_code == requests.codes.ok:
            try:
                json_response = json.loads(response.text)
                return json_response
            except json.JSONDecodeError as e:
                logging.error('Parsing response for card %s failed',
                              trello_card.id)
                logging.error(e)
                logging.error('Exiting program.')
                sys.exit(
                    f'Parsing response for card {trello_card.id} failed'
                )
        else:
            logging.error('Request for card %s failed', trello_card.id)
            logging.error('Exiting program now.')
            sys.exit(f'Request for card {trello_card.id} failed')

    def create_list(self, trello_list):
        """Creates a List associated with the provided Trello Board ID"""
        querystring = {
            "name": trello_list.name,
            "idBoard": trello_list.board_id,
            "key": self.api_key,
            "token": self.api_token
        }
        response = self.make_request(
            "POST",
            "lists",
            querystring
        )
        if response.status_code == requests.codes.ok:
            try:
                json_response = json.loads(response.text)
                return json_response
            except json.JSONDecodeError as e:
                logging.error('Parsing response for list creation failed')
                logging.error(e)
                logging.error('Exiting program.')
                sys.exit(
                    'Parsing response for list creation failed'
                )
        else:
            logging.error('Request for list creation failed')
            logging.error('Exiting program now.')
            sys.exit('Request for list creation failed')

    def create_label(self, trello_label, trello_board):
        """Creates a Label associated with the provided Trello Board ID"""
        querystring = {
            "name": trello_label.name,
            "color": trello_label.color,
            "idBoard": trello_board.id,
            "key": self.api_key,
            "token": self.api_token
        }
        response = self.make_request(
            "POST",
            "labels",
            querystring
        )
        if response.status_code == requests.codes.ok:
            try:
                json_response = json.loads(response.text)
                return json_response
            except json.JSONDecodeError as e:
                logging.error('Parsing response for label creation failed')
                logging.error(e)
                logging.error('Exiting program.')
                sys.exit(
                    'Parsing response for label creation failed'
                )
        else:
            logging.error('Request for label creation failed')
            logging.error('Exiting program now.')
            sys.exit('Request for label creation failed')

    def create_comment(self, comment_text, trello_card):
        """Creates a Comment associated with the provided trello Card ID"""
        querystring = {
            "text": comment_text,
            "key": self.api_key,
            "token": self.api_token
        }
        response = self.make_request(
            "POST",
            f"cards/{trello_card.id}/actions/comments",
            querystring
        )
        if response.status_code == requests.codes.ok:
            try:
                json_response = json.loads(response.text)
                return json_response
            except json.JSONDecodeError as e:
                logging.error('Parsing response for comment creation failed')
                logging.error(e)
                logging.error('Exiting program.')
                sys.exit(
                    'Parsing response for comment creation failed'
                )
        else:
            logging.error('Request for comment creation failed')
            logging.error('Exiting program now.')
            sys.exit('Request for comment creation failed')

    def create_card(self, trello_list, trello_card):
        """Creates a Card associated with the provided Trello List ID and
        optional Label IDs """
        querystring = {
            "idList": trello_list.id,
            "keepFromSource": "all",
            "key": self.api_key,
            "token": self.api_token
        }
        if trello_card.labels is not None:
            querystring['idLabels'] = trello_card.labels
        if trello_card.name is not None:
            querystring['name'] = trello_card.name
        response = self.make_request(
            "POST",
            "cards",
            querystring
        )
        if response.status_code == requests.codes.ok:
            try:
                json_response = json.loads(response.text)
                return json_response
            except json.JSONDecodeError as e:
                logging.error('Parsing response for card creation failed')
                logging.error(e)
                logging.error('Exiting program.')
                sys.exit(
                    'Parsing response for card creation failed'
                )
        else:
            logging.error('Request for card creation failed')
            logging.error('Exiting program now.')
            sys.exit('Request for card creation failed')

    def make_request(self, method, request_path, arguments=None, files=None):
        logging.info('Handling request with method %s and path %s',
                     method, request_path)
        joined_url = '%s/%s/%s' % (
            self.base_api_url,
            self.api_version,
            request_path
        )
        try:
            response = requests.request(
                method,
                joined_url,
                params=arguments,
                timeout=1.50
            )
            logging.info('Request response: %s', str(response.status_code))
            return response
        except requests.exceptions.Timeout as e:
            logging.error('Connection to Trello API timed out.')
            logging.error(e)
            logging.error('Exiting program.')
            sys.exit('Connection to Trello API timed out.')
