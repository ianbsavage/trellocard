# -*- coding: utf-8 -*-

from .context import trellocard
import unittest
import os
import json

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
TEST_DATA_FILE = os.path.join(ROOT_DIR, '../data/test_config.json')


class BasicTestSuite(unittest.TestCase):
    """Basic test cases."""

    def setUp(self):
        self.api_key = os.environ['TRELLO_API_KEY']
        self.api_token = os.environ['TRELLO_API_TOKEN']
        print(TEST_DATA_FILE)
        with open(TEST_DATA_FILE) as json_file:
            test_data = json.loads(json_file.read())
        self.trello_board = trellocard.trelloboard.TrelloBoard(
            test_data['board_id'], 'test')
        self.trello_list = trellocard.trellolist.TrelloList(
            test_data['list_id'],
            self.trello_board.id,
            "testlist"
        )
        self.test_client = trellocard.trelloclient.TrelloClient(
            self.api_key,
            self.api_token)

    def test_absolute_truth_and_meaning(self):
        assert True

    def test_create_label(self):
        label = trellocard.trellolabel.TrelloLabel(None, 'testlabel', 'red')
        response = self.test_client.create_label(label, self.trello_board)
        assert response['name'] == 'testlabel' and response['color'] == 'red'

    def test_create_card_no_label(self):
        card = trellocard.trellocard.TrelloCard("testcard", None, None)
        response = self.test_client.create_card(self.trello_list, card)
        assert response['name'] == 'testcard' \
            and response['idList'] == self.trello_list.id

    def test_create_card_with_label(self):
        label = trellocard.trellolabel.TrelloLabel(None, 'testlabel', 'red')
        response = self.test_client.create_label(label, self.trello_board)
        label_id = response['id']
        card = trellocard.trellocard.TrelloCard(
            "testcard", label_id, None)
        response = self.test_client.create_card(self.trello_list, card)
        assert response['name'] == 'testcard' \
            and response['idLabels'][0] == label_id


if __name__ == '__main__':
    unittest.main()
