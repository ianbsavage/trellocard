Trello Card
===========

Simple project for adding a card to a Trello board.

Not intended for use in production.

Written with Python 3.7.4 in mind.

Installation
------------

Using pipenv:

.. code-block::

   trellocard$ pipenv install

Obtain an API token `here <https://trello.com/1/authorize?expiration=1day&name=MyPersonalToken&scope=read,write&response_type=token&key=d74a1ce2a3bc7a39ed7ff82d90d15c01>`_.

Set the token as the value of an environment variable named 'TRELLO_API_TOKEN'

Create environment variable 'TRELLO_API_KEY' with value 'd74a1ce2a3bc7a39ed7ff82d90d15c01'

Usage
-----
usage: __main__.py [-h] [--list_id LIST_ID] [--board_id BOARD_ID]
                   [--card_name CARD_NAME] [--card_comment CARD_COMMENT]
                   [--card_label_ids CARD_LABEL_IDS]
                   [--card_label_json CARD_LABEL_JSON] [--log_level LOG_LEVEL]
                   [--list_name LIST_NAME]

A simple program for adding a card to a Trello board

optional arguments:
  -h, --help            show this help message and exit
  --list_id LIST_ID     list ID where the card will be created
  --board_id BOARD_ID   board ID used if a list does not already exist
  --card_name CARD_NAME
                        name for the card that will be created
  --card_comment CARD_COMMENT
                        comment string added to a new card
  --card_label_ids CARD_LABEL_IDS
                        label IDs added to a new card. Additional values
                        separated by commas
  --card_label_json CARD_LABEL_JSON
                        JSON used to create new labels. Ex:
                        [{"name":"exlabel1","color":"blue"},...]
  --log_level LOG_LEVEL
                        log level from DEBUG, INFO, WARNING, ERROR or CRITICAL
  --list_name LIST_NAME
                        name for a list that will be created

Note: A log directory will be created at trellocard/logs. New log files are created upon each run, using the timestamp as the filename.

Examples
--------

Create a blank card in a list
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block::

   trellocard$ python -m trellocard --list_id <list id>

Create a new list and a blank card in a board
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block::

   trellocard$ python -m trellocard --board_id <board id> --list_name <list name>

Create a card with a comment and a label
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block::

   trellocard$ python -m trellocard --list_id <list id> --card_name <card name> --card_label_ids <label ids> --card_comment "<card comment>"

Create a card with a comment and a label JSON string
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block::

   trellocard$ python -m trellocard --list_id <list id> --card_name <card name> --card_label_json "[{\"name\":\"<label name>\",\"color\":\"<label color>\"},...]" --card_comment "<card comment>"
